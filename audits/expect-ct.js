'use strict';
const Audit = require('lighthouse').Audit;

class ExpectsCtAudit extends Audit {
  static get meta() {
    return {
      id: 'expect-ct',
      title: 'Expects Certificate Transparency (CT)',
      failureTitle: 'Is missing Expects CT header',
      category: 'Security',
      name: 'expect-ct',
      description: 'Expects Certificate Transparency (CT)',
      failureDescription: 'Is missing Expects CT header',
      helpText: 'When a site enables the Expect-CT header, they are requesting that the browser ' +
                'check that any certificate for that site appears in public CT logs. ' +
                '[Learn more](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Expect-CT)',
      requiredArtifacts: ['CtMetaGatherer', 'ResponseHeaders']
    };
  }

  static audit(artifacts) {
    const ctMetaTags = artifacts.CtMetaGatherer;
    const headers = artifacts.ResponseHeaders;
    const hasCtMetaTags = ctMetaTags.length > 0;
    const expectCtHeader = headers['expect-ct'];
    const hasCtHeader = !!expectCtHeader;
    const expectsCt = hasCtMetaTags || hasCtHeader;

    return {
      rawValue: expectsCt
    };
  }
}

module.exports = ExpectsCtAudit;
