'use strict';

const Gatherer = require('lighthouse').Gatherer;

class CtMetaGatherer extends Gatherer {
  afterPass(options) {
    const driver = options.driver;
    const metaSelector = 'meta[http-equiv=Expect-CT]';
    return driver.evaluateAsync(`window.document.querySelectorAll('${metaSelector}')`);
  }
}

module.exports = CtMetaGatherer;